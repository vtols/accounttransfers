plugins {
    java
    application
}

group = "transfers.app"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation("org.slf4j", "slf4j-simple", "1.8.0-beta4")
    implementation("io.javalin", "javalin", "3.7.0")
    implementation("com.fasterxml.jackson.core", "jackson-databind", "2.10.1")
    implementation("com.h2database", "h2", "1.4.200")
    implementation("com.zaxxer", "HikariCP", "3.4.2")
    implementation("org.flywaydb", "flyway-core", "6.2.4")
    implementation("org.jooq", "jooq", "3.13.1")
    testImplementation("org.mockito", "mockito-core", "3.3.0")
    testImplementation("org.junit.jupiter", "junit-jupiter-api", "5.6.0")
    testImplementation("com.konghq", "unirest-java", "3.6.00")
    testRuntime("org.junit.jupiter", "junit-jupiter-engine", "5.6.0")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_11
}

application {
    mainClassName = "transfers.app.AccountApp"
}

tasks.test {
    useJUnitPlatform()
}