# Account Transfers

## Description

This project demonstrates application with API to store accounts and transfer funds between them.

Technologies:

* H2 as in-memory storage
* HikariCP as connection pool
* Javalin for API
* jOOQ for building and running SQL request
* Junit5 and Mockito for testing
* Unirest for API tests


## Running application and test

To run application use
```
gradle :run
```

To run tests
```
gradle :test
```