package transfers.app.dao;

import transfers.app.function.ThrowingConsumer;


public class MockedConfigurationWrapper implements ConfigurationWrapper {
    private ContextProvider contextProvider;

    public MockedConfigurationWrapper(ContextProvider contextProvider) {
        this.contextProvider = contextProvider;
    }

    @Override
    public ContextProvider getContextProvider() {
        return contextProvider;
    }

    @Override
    public <E extends Exception> void withTransaction(ThrowingConsumer<ConfigurationWrapper, E> transactionActions) throws E {
        transactionActions.accept(this);
    }

}