package transfers.app;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import transfers.app.modules.ApplicationModule;

import java.math.BigDecimal;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AccountAppTest {

    private static final ApplicationModule app = new ApplicationModule();

    private static final String APPLICATION_URL = "http://localhost:8000";

    private static final String CREATE_ENDPOINT = APPLICATION_URL + "/account/create";

    private static final String TRANSFER_ENDPOINT = APPLICATION_URL + "/account/transfer";

    private static final String GET_ENDPOINT = APPLICATION_URL + "/account/{id}";

    @BeforeAll
    static void startApplication() {
        app.start();
    }

    @AfterAll
    static void stopApplication() {
        app.stop();
    }

    @Test
    void createNewAccount() {
        BigDecimal initialAmount = new BigDecimal("25.10");
        String createRequestBody = String.format("{\"amount\": %f}", initialAmount);

        HttpResponse<JsonNode> createResponse =
            Unirest.post(CREATE_ENDPOINT)
                .body(createRequestBody)
                .asJson();

        assertEquals(201, createResponse.getStatus());

        JsonNode createResponseBody = createResponse.getBody();
        BigDecimal responseAmount = createResponseBody.getObject().getBigDecimal("amount");

        assertEquals(initialAmount, responseAmount);
    }

    @Test
    void requestExistingAccount() {
        BigDecimal initialAmount = new BigDecimal("25.10");
        String createRequestBody = String.format("{\"amount\": %f}", initialAmount);

        HttpResponse<JsonNode> createResponse =
                Unirest.post(CREATE_ENDPOINT)
                        .body(createRequestBody)
                        .asJson();

        assertEquals(201, createResponse.getStatus());

        JsonNode createResponseBody = createResponse.getBody();
        UUID accountId = UUID.fromString(createResponseBody.getObject().getString("id"));

        HttpResponse<JsonNode> getResponse =
                Unirest.get(GET_ENDPOINT)
                        .routeParam("id", accountId.toString())
                        .asJson();

        assertEquals(200, getResponse.getStatus());

        JsonNode getResponseBody = getResponse.getBody();
        UUID responseId = UUID.fromString(getResponseBody.getObject().getString("id"));
        BigDecimal responseAmount = getResponseBody.getObject().getBigDecimal("amount");

        assertEquals(accountId, responseId);
        assertEquals(initialAmount, responseAmount);
    }

    @Test
    void transferBetweenAccounts() {
        BigDecimal initialAmount = new BigDecimal("25.10");
        String createRequestBody = String.format("{\"amount\": %f}", initialAmount);

        HttpResponse<JsonNode> createFirstResponse =
                Unirest.post(CREATE_ENDPOINT)
                        .body(createRequestBody)
                        .asJson();

        assertEquals(201, createFirstResponse.getStatus());

        JsonNode createFirstResponseBody = createFirstResponse.getBody();
        UUID firstId = UUID.fromString(createFirstResponseBody.getObject().getString("id"));

        HttpResponse<JsonNode> createSecondResponse =
                Unirest.post(CREATE_ENDPOINT)
                        .body(createRequestBody)
                        .asJson();

        assertEquals(201, createSecondResponse.getStatus());

        JsonNode createSecondResponseBody = createSecondResponse.getBody();
        UUID secondId = UUID.fromString(createSecondResponseBody.getObject().getString("id"));

        BigDecimal transferAmount = new BigDecimal("5.00");
        String transferRequestBody =
            String.format(
                    "{\"from\": \"%s\", \"to\": \"%s\", \"amount\": %f}",
                    firstId.toString(),
                    secondId.toString(),
                    transferAmount);

        HttpResponse<String> transferResponse =
                Unirest.post(TRANSFER_ENDPOINT)
                        .body(transferRequestBody)
                        .asString();

        assertEquals(200, transferResponse.getStatus());
        assertEquals("OK", transferResponse.getBody());

        HttpResponse<JsonNode> firstGetResponse =
                Unirest.get(GET_ENDPOINT)
                        .routeParam("id", firstId.toString())
                        .asJson();

        assertEquals(200, firstGetResponse.getStatus());

        JsonNode firstGetResponseBody = firstGetResponse.getBody();
        UUID firstGetResponseId = UUID.fromString(firstGetResponseBody.getObject().getString("id"));
        BigDecimal firstGetResponseAmount = firstGetResponseBody.getObject().getBigDecimal("amount");

        assertEquals(firstId, firstGetResponseId);
        assertEquals(new BigDecimal("20.10"), firstGetResponseAmount);

        HttpResponse<JsonNode> secondGetResponse =
                Unirest.get(GET_ENDPOINT)
                        .routeParam("id", secondId.toString())
                        .asJson();

        assertEquals(200, secondGetResponse.getStatus());

        JsonNode secondGetResponseBody = secondGetResponse.getBody();
        UUID secondGetResponseId = UUID.fromString(secondGetResponseBody.getObject().getString("id"));
        BigDecimal secondGetResponseAmount = secondGetResponseBody.getObject().getBigDecimal("amount");

        assertEquals(secondId, secondGetResponseId);
        assertEquals(new BigDecimal("30.10"), secondGetResponseAmount);
    }

}