package transfers.app.service;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import transfers.app.dao.AccountDao;
import transfers.app.dao.ConfigurationWrapper;
import transfers.app.model.Account;
import transfers.app.model.AccountDto;
import transfers.app.modules.AccountModule;
import transfers.app.modules.DatabaseModule;
import transfers.app.service.error.AccountServiceException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class ConcurrentAccountUpdatesTest {

    private static final DatabaseModule databaseModule = new DatabaseModule();

    private static final AccountModule accountModule = new AccountModule(databaseModule);

    private static final AccountService accountService = accountModule.accountService;

    private static final AccountDao accountDao = accountModule.accountDao;

    private static final ConfigurationWrapper context = databaseModule.configWrapper;

    private static final int TASK_COUNT = 200;

    private static final int THREAD_COUNT = 8;

    private static final ExecutorService executorService = Executors.newFixedThreadPool(THREAD_COUNT);


    @BeforeAll
    static void startDatabase() {
        databaseModule.initializeDatabase();
    }

    @AfterAll
    static void stopDatabase() {
        databaseModule.shutdownDatabase();
        executorService.shutdown();
    }

    @Test
    public void concurrentTransfersTest() throws AccountServiceException {
        Account firstAccount = accountService.createAccount(BigDecimal.valueOf(TASK_COUNT * 3));
        Account secondAccount = accountService.createAccount(BigDecimal.valueOf(TASK_COUNT * 3));
        Account thirdAccount = accountService.createAccount(BigDecimal.valueOf(TASK_COUNT * 3));

        List<Callable<Integer>> transferTasks = new ArrayList<>();
        for (int i = 0; i < TASK_COUNT; i++) {
            transferTasks.add(makeTask(firstAccount, secondAccount));
            transferTasks.add(makeTask(secondAccount, firstAccount));

            transferTasks.add(makeTask(firstAccount, thirdAccount));
            transferTasks.add(makeTask(thirdAccount, firstAccount));

            transferTasks.add(makeTask(secondAccount, thirdAccount));
            transferTasks.add(makeTask(thirdAccount, secondAccount));
        }
        boolean waitSuccess = waitTasksToComplete(transferTasks);
        assertTrue(waitSuccess, "Expected tasks to complete successfully");

        Optional<AccountDto> firstAccountUpdatedOptional = accountDao.findById(context.getContextProvider(), firstAccount.getId());
        Optional<AccountDto> secondAccountUpdatedOptional = accountDao.findById(context.getContextProvider(), secondAccount.getId());
        Optional<AccountDto> thirdAccountUpdatedOptional = accountDao.findById(context.getContextProvider(), thirdAccount.getId());

        assertTrue(firstAccountUpdatedOptional.isPresent());
        assertTrue(secondAccountUpdatedOptional.isPresent());
        assertTrue(thirdAccountUpdatedOptional.isPresent());

        AccountDto firstAccountUpdated = firstAccountUpdatedOptional.get();
        AccountDto secondAccountUpdated = secondAccountUpdatedOptional.get();
        AccountDto thirdAccountUpdated = thirdAccountUpdatedOptional.get();

        assertEquals(BigDecimal.valueOf(TASK_COUNT * 300, 2), firstAccountUpdated.getAmount());
        assertEquals(BigDecimal.valueOf(TASK_COUNT * 300, 2), secondAccountUpdated.getAmount());
        assertEquals(BigDecimal.valueOf(TASK_COUNT * 300, 2), thirdAccountUpdated.getAmount());

        assertEquals(TASK_COUNT * 4, firstAccountUpdated.getVersion());
        assertEquals(TASK_COUNT * 4, secondAccountUpdated.getVersion());
        assertEquals(TASK_COUNT * 4, thirdAccountUpdated.getVersion());
    }

    private Callable<Integer> makeTask(Account from, Account to) {
        return () -> {
            accountService.transfer(from.getId(), to.getId(), new BigDecimal("1.00"));
            return 0;
        };
    }

    private boolean waitTasksToComplete(List<Callable<Integer>> tasks) {
        List<Future<Integer>> taskResults;
        try {
            taskResults = executorService.invokeAll(tasks);
            for (Future<Integer> taskResult : taskResults) {
                taskResult.get();
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

}
