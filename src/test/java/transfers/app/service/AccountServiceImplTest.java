package transfers.app.service;

import org.junit.jupiter.api.Test;
import transfers.app.dao.AccountDao;
import transfers.app.dao.ConfigurationWrapper;
import transfers.app.dao.ContextProvider;
import transfers.app.dao.IdGenerator;
import transfers.app.dao.MockedConfigurationWrapper;
import transfers.app.model.Account;
import transfers.app.model.AccountDto;
import transfers.app.service.error.AccountServiceException;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AccountServiceImplTest {

    private final AccountDao mockedAccountDao = mock(AccountDao.class);

    private final ContextProvider mockedContextProvider = mock(ContextProvider.class);

    private final ConfigurationWrapper mockedConfig = new MockedConfigurationWrapper(mockedContextProvider);

    private final IdGenerator mockedIdGenerator = mock(IdGenerator.class);

    private final AccountService accountService = new AccountServiceImpl(mockedAccountDao, mockedConfig, mockedIdGenerator);

    @Test
    public void cannotCreateAccountWithNegativeAmount() {
        BigDecimal initialAmount = BigDecimal.ONE.negate();

        Exception exception = assertThrows(AccountServiceException.class,
            () -> accountService.createAccount(initialAmount));

        String expectedMessage = "Initial amount cannot be negative";
        assertEquals(expectedMessage, exception.getMessage());
    }

    @Test
    public void cannotCreateAccountGreaterMaxAmount() {
        BigDecimal initialAmount = new BigDecimal("1000000000.00");

        Exception exception = assertThrows(AccountServiceException.class,
                () -> accountService.createAccount(initialAmount));

        String expectedMessage = "Initial amount cannot be greater than max allowed amount";
        assertEquals(expectedMessage, exception.getMessage());
    }

    @Test
    public void createAccount() throws AccountServiceException {
        BigDecimal initialAmount = new BigDecimal("10.00");

        UUID newAccountId = UUID.fromString("61e99917-8156-4ddc-98fe-c85e3b61ebb9");

        when(mockedIdGenerator.generateNewId()).thenReturn(newAccountId);
        when(mockedAccountDao.create(mockedContextProvider, newAccountId, initialAmount))
            .thenAnswer(args -> new AccountDto(newAccountId, initialAmount, 0L));

        Account createdAccount = accountService.createAccount(initialAmount);

        assertEquals(newAccountId, createdAccount.getId());
        assertEquals(initialAmount, createdAccount.getAmount());
    }

    @Test
    public void createAccountAmountRounding() throws AccountServiceException {
        BigDecimal initialAmount = new BigDecimal("10.256");
        BigDecimal initialAmountRounded = new BigDecimal("10.25");

        UUID newAccountId = UUID.fromString("61e99917-8156-4ddc-98fe-c85e3b61ebb9");

        when(mockedIdGenerator.generateNewId()).thenReturn(newAccountId);
        when(mockedAccountDao.create(eq(mockedContextProvider), eq(newAccountId), any(BigDecimal.class)))
            .thenAnswer(args -> new AccountDto(newAccountId, args.getArgument(2, BigDecimal.class), 0L));

        Account createdAccount = accountService.createAccount(initialAmount);

        assertEquals(newAccountId, createdAccount.getId());
        assertEquals(initialAmountRounded, createdAccount.getAmount());
    }

    @Test
    public void getExistingAccount() throws AccountServiceException {
        UUID accountId = UUID.fromString("61e99917-8156-4ddc-98fe-c85e3b61ebb9");
        AccountDto existingAccount = new AccountDto(accountId, BigDecimal.ZERO, 0L);

        when(mockedAccountDao.findById(mockedContextProvider, accountId))
            .thenReturn(Optional.of(existingAccount));

        Optional<Account> resultAccountOptional = accountService.getAccount(accountId);
        assertTrue(resultAccountOptional.isPresent());

        Account resultAccount = resultAccountOptional.get();

        assertEquals(existingAccount.getId(), resultAccount.getId());
        assertEquals(existingAccount.getAmount(), resultAccount.getAmount());
    }

    @Test
    public void getNotExistingAccount() throws AccountServiceException {
        UUID accountId = UUID.fromString("61e99917-8156-4ddc-98fe-c85e3b61ebb9");

        when(mockedAccountDao.findById(mockedContextProvider, accountId))
                .thenReturn(Optional.empty());

        Optional<Account> resultAccountOptional = accountService.getAccount(accountId);
        assertTrue(resultAccountOptional.isEmpty());
    }

    @Test
    public void cannotTransferToTheSameAccount() {
        UUID sourceId = UUID.fromString("61e99917-8156-4ddc-98fe-c85e3b61ebb9");
        UUID targetId = UUID.fromString("61e99917-8156-4ddc-98fe-c85e3b61ebb9");
        BigDecimal amount = BigDecimal.ONE;

        Exception exception = assertThrows(AccountServiceException.class,
                () -> accountService.transfer(sourceId, targetId, amount));

        String expectedMessage = "Transfer to the same account is impossible";
        assertEquals(expectedMessage, exception.getMessage());
    }

    @Test
    public void cannotTransferNegativeAmount() {
        UUID sourceId = UUID.fromString("61e99917-8156-4ddc-98fe-c85e3b61ebb9");
        UUID targetId = UUID.fromString("1dc287fe-5675-11ea-9420-a71feaaff59b");
        BigDecimal amount = BigDecimal.ONE.negate();

        Exception exception = assertThrows(AccountServiceException.class,
                () -> accountService.transfer(sourceId, targetId, amount));

        String expectedMessage = "Transfer amount cannot be zero or negative";
        assertEquals(expectedMessage, exception.getMessage());
    }

    @Test
    public void cannotTransferZeroAmount() {
        UUID sourceId = UUID.fromString("61e99917-8156-4ddc-98fe-c85e3b61ebb9");
        UUID targetId = UUID.fromString("1dc287fe-5675-11ea-9420-a71feaaff59b");
        BigDecimal amount = BigDecimal.ZERO;

        Exception exception = assertThrows(AccountServiceException.class,
                () -> accountService.transfer(sourceId, targetId, amount));

        String expectedMessage = "Transfer amount cannot be zero or negative";
        assertEquals(expectedMessage, exception.getMessage());
    }

    @Test
    public void cannotTransferGreaterMaxAmount() {
        UUID sourceId = UUID.fromString("61e99917-8156-4ddc-98fe-c85e3b61ebb9");
        UUID targetId = UUID.fromString("1dc287fe-5675-11ea-9420-a71feaaff59b");
        BigDecimal amount = new BigDecimal("1000000000.00");

        Exception exception = assertThrows(AccountServiceException.class,
                () -> accountService.transfer(sourceId, targetId, amount));

        String expectedMessage = "Transfer amount cannot be greater than max allowed amount";
        assertEquals(expectedMessage, exception.getMessage());
    }

    @Test
    public void cannotTransferToNonExistentAccount() {
        UUID sourceId = UUID.fromString("61e99917-8156-4ddc-98fe-c85e3b61ebb9");
        UUID targetId = UUID.fromString("1dc287fe-5675-11ea-9420-a71feaaff59b");
        BigDecimal amount = BigDecimal.ONE;

        when(mockedAccountDao.exists(mockedConfig.getContextProvider(), sourceId))
            .thenReturn(true);

        Exception exception = assertThrows(AccountServiceException.class,
                () -> accountService.transfer(sourceId, targetId, amount));

        String expectedMessage = "Account id=" + targetId + " not found";
        assertEquals(expectedMessage, exception.getMessage());
    }

    @Test
    public void cannotTransferFromNonExistentAccount() {
        UUID sourceId = UUID.fromString("61e99917-8156-4ddc-98fe-c85e3b61ebb9");
        UUID targetId = UUID.fromString("1dc287fe-5675-11ea-9420-a71feaaff59b");
        BigDecimal amount = BigDecimal.ONE;

        Account targetAccount = new Account(targetId, BigDecimal.ZERO);
        when(mockedAccountDao.exists(mockedConfig.getContextProvider(), targetId))
            .thenReturn(true);

        Exception exception = assertThrows(AccountServiceException.class,
                () -> accountService.transfer(sourceId, targetId, amount));

        String expectedMessage = "Account id=" + sourceId + " not found";
        assertEquals(expectedMessage, exception.getMessage());
    }

    @Test
    public void cannotTransferIfNotEnoughFunds() {
        UUID sourceId = UUID.fromString("61e99917-8156-4ddc-98fe-c85e3b61ebb9");
        UUID targetId = UUID.fromString("1dc287fe-5675-11ea-9420-a71feaaff59b");
        BigDecimal amount = BigDecimal.ONE;

        AccountDto sourceAccount = new AccountDto(sourceId, BigDecimal.ZERO, 0L);
        when(mockedAccountDao.exists(mockedConfig.getContextProvider(), sourceId))
            .thenReturn(true);


        AccountDto targetAccount = new AccountDto(targetId, BigDecimal.ZERO, 0L);
        when(mockedAccountDao.exists(mockedConfig.getContextProvider(), targetId))
            .thenReturn(true);

        when(mockedAccountDao.findForUpdate(mockedConfig.getContextProvider(), sourceId, targetId))
            .thenReturn(List.of(sourceAccount, targetAccount));

        Exception exception = assertThrows(AccountServiceException.class,
                () -> accountService.transfer(sourceId, targetId, amount));

        String expectedMessage = String.format("Not enough funds to transfer %f from %s", amount, sourceId);
        assertEquals(expectedMessage, exception.getMessage());
    }

    @Test
    public void cannotTransferTotalTargetAmountTooBig() {
        UUID sourceId = UUID.fromString("61e99917-8156-4ddc-98fe-c85e3b61ebb9");
        UUID targetId = UUID.fromString("1dc287fe-5675-11ea-9420-a71feaaff59b");
        BigDecimal amount = new BigDecimal("999999999.99");

        AccountDto sourceAccount = new AccountDto(sourceId, amount, 0L);
        when(mockedAccountDao.exists(mockedConfig.getContextProvider(), sourceId))
            .thenReturn(true);

        AccountDto targetAccount = new AccountDto(targetId, amount, 0L);
        when(mockedAccountDao.exists(mockedConfig.getContextProvider(), targetId))
            .thenReturn(true);

        when(mockedAccountDao.findForUpdate(mockedConfig.getContextProvider(), sourceId, targetId))
            .thenReturn(List.of(sourceAccount, targetAccount));

        Exception exception = assertThrows(AccountServiceException.class,
                () -> accountService.transfer(sourceId, targetId, amount));

        String expectedMessage = "Total amount on target account " + targetId + " cannot be grater than max allowed amount";
        assertEquals(expectedMessage, exception.getMessage());
    }

    @Test
    public void transferFromTo() throws AccountServiceException {
        UUID sourceId = UUID.fromString("61e99917-8156-4ddc-98fe-c85e3b61ebb9");
        UUID targetId = UUID.fromString("1dc287fe-5675-11ea-9420-a71feaaff59b");
        BigDecimal initialAmount = new BigDecimal("10.00");
        BigDecimal transferAmount = new BigDecimal("5.00");

        AccountDto sourceAccount = new AccountDto(sourceId, initialAmount, 0L);
        when(mockedAccountDao.exists(mockedConfig.getContextProvider(), sourceId))
                .thenReturn(true);

        AccountDto targetAccount = new AccountDto(targetId, initialAmount, 0L);
        when(mockedAccountDao.exists(mockedConfig.getContextProvider(), targetId))
                .thenReturn(true);

        when(mockedAccountDao.findForUpdate(mockedConfig.getContextProvider(), sourceId, targetId))
                .thenReturn(List.of(sourceAccount, targetAccount));

        when(mockedAccountDao.updateAmount(eq(mockedContextProvider), any(), any()))
                .thenReturn(true);

        accountService.transfer(sourceId, targetId, transferAmount);

        verify(mockedAccountDao, times(1)).updateAmount(mockedContextProvider, sourceAccount, new BigDecimal("5.00"));
        verify(mockedAccountDao, times(1)).updateAmount(mockedContextProvider, targetAccount, new BigDecimal("15.00"));
    }

    @Test
    public void transferFromToRounding() throws AccountServiceException {
        UUID sourceId = UUID.fromString("61e99917-8156-4ddc-98fe-c85e3b61ebb9");
        UUID targetId = UUID.fromString("1dc287fe-5675-11ea-9420-a71feaaff59b");
        BigDecimal initialAmount = new BigDecimal("10.00");
        BigDecimal transferAmount = new BigDecimal("10.001");

        AccountDto sourceAccount = new AccountDto(sourceId, initialAmount, 0L);
        when(mockedAccountDao.exists(mockedConfig.getContextProvider(), sourceId))
                .thenReturn(true);

        AccountDto targetAccount = new AccountDto(targetId, initialAmount, 0L);
        when(mockedAccountDao.exists(mockedConfig.getContextProvider(), targetId))
                .thenReturn(true);

        when(mockedAccountDao.findForUpdate(mockedConfig.getContextProvider(), sourceId, targetId))
                .thenReturn(List.of(sourceAccount, targetAccount));

        when(mockedAccountDao.updateAmount(eq(mockedContextProvider), any(), any()))
                .thenReturn(true);

        accountService.transfer(sourceId, targetId, transferAmount);

        verify(mockedAccountDao, times(1)).updateAmount(mockedContextProvider, sourceAccount, new BigDecimal("0.00"));
        verify(mockedAccountDao, times(1)).updateAmount(mockedContextProvider, targetAccount, new BigDecimal("20.00"));
    }

}