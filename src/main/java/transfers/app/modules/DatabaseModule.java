package transfers.app.modules;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.flywaydb.core.Flyway;
import org.jooq.Configuration;
import org.jooq.SQLDialect;
import org.jooq.impl.DefaultConfiguration;
import transfers.app.dao.ConfigurationWrapper;
import transfers.app.dao.ConfigurationWrapperImpl;

public class DatabaseModule {

    public void initializeDatabase() {
        flyway.migrate();
    }

    public void shutdownDatabase() {
        dataSource.close();
    }

    public final String databaseUrl = "jdbc:h2:mem:accounts_db";

    public final HikariDataSource dataSource;

    public final Configuration configuration;

    private final Flyway flyway;

    public final ConfigurationWrapper configWrapper;

    public DatabaseModule() {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl(databaseUrl);
        hikariConfig.setTransactionIsolation("TRANSACTION_READ_COMMITTED");
        hikariConfig.setMinimumIdle(1);

        dataSource = new HikariDataSource(hikariConfig);
        flyway = Flyway.configure().dataSource(dataSource).load();

        configuration =
            new DefaultConfiguration()
                    .set(dataSource)
                    .set(SQLDialect.H2);

        configWrapper = new ConfigurationWrapperImpl(configuration);
    }

}
