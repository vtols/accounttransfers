package transfers.app.modules;

import io.javalin.Javalin;
import transfers.app.controller.AccountController;
import transfers.app.service.error.AccountServiceException;

public class ApiModule {

    private final Javalin javalinApp = Javalin.create();

    public ApiModule(AccountModule accountModule) {
        AccountController controller = accountModule.accountController;

        javalinApp.post("/account/create", controller.createAccount);
        javalinApp.post("/account/transfer", controller.transferBetweenAccounts);
        javalinApp.get("/account/:id", controller.getAccount);

        javalinApp.exception(AccountServiceException.class, controller.accountServiceExceptions);
    }

    public void start() {
        javalinApp.start("localhost", 8000);
    }

    public void stop() {
        javalinApp.stop();
    }

}
