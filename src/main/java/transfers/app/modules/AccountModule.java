package transfers.app.modules;

import transfers.app.controller.AccountController;
import transfers.app.dao.AccountDao;
import transfers.app.dao.AccountDaoImpl;
import transfers.app.dao.IdGenerator;
import transfers.app.service.AccountService;
import transfers.app.service.AccountServiceImpl;

public class AccountModule {

    public final AccountDao accountDao = new AccountDaoImpl();

    public final AccountService accountService;

    public final AccountController accountController;

    public AccountModule(DatabaseModule databaseModule) {
        accountService = new AccountServiceImpl(accountDao, databaseModule.configWrapper, IdGenerator.DEFAULT_ID_GENERATOR);
        accountController = new AccountController(accountService);
    }

}
