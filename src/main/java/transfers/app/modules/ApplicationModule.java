package transfers.app.modules;

public class ApplicationModule {

    public final DatabaseModule databaseModule;

    public final AccountModule accountModule;

    public final ApiModule apiModule;

    public ApplicationModule() {
        databaseModule = new DatabaseModule();
        accountModule = new AccountModule(databaseModule);
        apiModule = new ApiModule(accountModule);
    }

    public void start() {
        databaseModule.initializeDatabase();
        apiModule.start();
    }

    public void stop() {
        apiModule.stop();
        databaseModule.shutdownDatabase();
    }

}
