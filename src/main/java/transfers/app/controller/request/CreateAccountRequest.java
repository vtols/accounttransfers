package transfers.app.controller.request;

import java.math.BigDecimal;

public class CreateAccountRequest {
    private BigDecimal amount;

    public CreateAccountRequest() {}

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}
