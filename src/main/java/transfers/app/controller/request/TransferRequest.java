package transfers.app.controller.request;

import java.math.BigDecimal;
import java.util.UUID;

public class TransferRequest {
    private UUID from;

    private UUID to;

    private BigDecimal amount;

    public UUID getFrom() {
        return from;
    }

    public void setFrom(UUID from) {
        this.from = from;
    }

    public UUID getTo() {
        return to;
    }

    public void setTo(UUID to) {
        this.to = to;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
