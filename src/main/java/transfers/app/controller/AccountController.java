package transfers.app.controller;

import io.javalin.http.ExceptionHandler;
import io.javalin.http.Handler;
import transfers.app.controller.request.CreateAccountRequest;
import transfers.app.controller.request.TransferRequest;
import transfers.app.controller.response.ErrorResponse;
import transfers.app.model.Account;
import transfers.app.service.AccountService;
import transfers.app.service.error.AccountServiceException;

import javax.servlet.http.HttpServletResponse;
import java.util.Optional;
import java.util.UUID;

public class AccountController {

    private AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    public final Handler createAccount = ctx -> {
        CreateAccountRequest request = ctx.bodyAsClass(CreateAccountRequest.class);
        Account newAccount = accountService.createAccount(request.getAmount());

        ctx.status(HttpServletResponse.SC_CREATED);
        ctx.json(newAccount);
    };

    public final Handler getAccount = ctx -> {
        UUID id = UUID.fromString(ctx.pathParam("id"));
        Optional<Account> foundAccount = accountService.getAccount(id);

        foundAccount.ifPresentOrElse(
                ctx::json,
                () -> {
                    ctx.status(HttpServletResponse.SC_NOT_FOUND);
                    ctx.json(new ErrorResponse("Account not found id=" + id));
                }
        );
    };

    public final Handler transferBetweenAccounts = ctx -> {
        TransferRequest request = ctx.bodyAsClass(TransferRequest.class);
        accountService.transfer(request.getFrom(), request.getTo(), request.getAmount());

        ctx.result("OK");
    };

    public final ExceptionHandler<AccountServiceException> accountServiceExceptions = (exception, ctx) -> {
        ErrorResponse response = new ErrorResponse(exception.getMessage());
        ctx.json(response);
    };

}
