package transfers.app.model;

import java.math.BigDecimal;
import java.util.UUID;

public class Account {

    private UUID id;

    private BigDecimal amount;

    public Account(UUID id, BigDecimal amount) {
        this.id = id;
        this.amount = amount;
    }

    public UUID getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

}