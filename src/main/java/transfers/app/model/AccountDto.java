package transfers.app.model;

import java.math.BigDecimal;
import java.util.UUID;

public class AccountDto {

    private UUID id;

    private BigDecimal amount;

    private long version;

    public AccountDto(UUID id, BigDecimal amount, Long version) {
        this.id = id;
        this.amount = amount;
        this.version = version;
    }

    public UUID getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public long getVersion() {
        return version;
    }

    public Account toAccount() {
        return new Account(id, amount);
    }

}
