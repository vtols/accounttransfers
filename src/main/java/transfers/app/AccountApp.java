package transfers.app;

import transfers.app.modules.ApplicationModule;

public class AccountApp {
    public static void main(String[] args) {
        ApplicationModule app = new ApplicationModule();
        app.start();
    }
}
