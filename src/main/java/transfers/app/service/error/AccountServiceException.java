package transfers.app.service.error;

import java.math.BigDecimal;
import java.util.UUID;

public class AccountServiceException extends Exception {

    public AccountServiceException(String message) {
        super(message);
    }

    public static final AccountServiceException INITIAL_AMOUNT_NEGATIVE =
        new AccountServiceException("Initial amount cannot be negative");

    public static final AccountServiceException INITIAL_AMOUNT_TOO_BIG =
        new AccountServiceException("Initial amount cannot be greater than max allowed amount");

    public static final AccountServiceException SAME_ACCOUNT_TRANSFER =
        new AccountServiceException("Transfer to the same account is impossible");

    public static final AccountServiceException NON_POSITIVE_TRANSFER_AMOUNT =
        new AccountServiceException("Transfer amount cannot be zero or negative");

    public static final AccountServiceException TRANSFER_AMOUNT_TOO_BIG =
        new AccountServiceException("Transfer amount cannot be greater than max allowed amount");

    public static final AccountServiceException ACCOUNT_FETCH_FAILED =
        new AccountServiceException("Failed to find source or target account");

    public static AccountServiceException accountNotFound(UUID accountId) {
        return new AccountServiceException("Account id=" + accountId + " not found");
    }

    public static AccountServiceException notEnoughFunds(UUID accountId, BigDecimal amount) {
        return new AccountServiceException(String.format("Not enough funds to transfer %f from %s", amount, accountId));
    }

    public static AccountServiceException totalTargetAmountTooBig(UUID accountId) {
        return new AccountServiceException("Total amount on target account " + accountId + " cannot be grater than max allowed amount");
    }

}
