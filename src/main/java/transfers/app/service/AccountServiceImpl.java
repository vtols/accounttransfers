package transfers.app.service;

import transfers.app.dao.AccountDao;
import transfers.app.dao.ConfigurationWrapper;
import transfers.app.dao.ContextProvider;
import transfers.app.dao.IdGenerator;
import transfers.app.function.ThrowingConsumer;
import transfers.app.model.Account;
import transfers.app.model.AccountDto;
import transfers.app.service.error.AccountServiceException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class AccountServiceImpl implements AccountService {

    private AccountDao accountDao;

    private ConfigurationWrapper config;

    private IdGenerator idGenerator;

    private static final BigDecimal MAX_AMOUNT = new BigDecimal("999999999.99");

    public AccountServiceImpl(AccountDao accountDao, ConfigurationWrapper config, IdGenerator idGenerator) {
        this.accountDao = accountDao;
        this.config = config;
        this.idGenerator = idGenerator;
    }

    @Override
    public Account createAccount(BigDecimal amount) throws AccountServiceException {
        if (amount.compareTo(BigDecimal.ZERO) < 0)
            throw AccountServiceException.INITIAL_AMOUNT_NEGATIVE;

        if (amount.compareTo(MAX_AMOUNT) > 0)
            throw AccountServiceException.INITIAL_AMOUNT_TOO_BIG;

        amount = amount.setScale(2, RoundingMode.FLOOR);

        UUID newId = idGenerator.generateNewId();

        return accountDao.create(config.getContextProvider(), newId, amount).toAccount();
    }

    @Override
    public Optional<Account> getAccount(UUID id) {
        return accountDao.findById(config.getContextProvider(), id).map(AccountDto::toAccount);
    }

    @Override
    public void transfer(UUID sourceId, UUID targetId, BigDecimal amount) throws AccountServiceException {
        if (sourceId.equals(targetId))
            throw AccountServiceException.SAME_ACCOUNT_TRANSFER;

        if (amount.compareTo(BigDecimal.ZERO) <= 0)
            throw AccountServiceException.NON_POSITIVE_TRANSFER_AMOUNT;

        if (amount.compareTo(MAX_AMOUNT) > 0)
            throw AccountServiceException.TRANSFER_AMOUNT_TOO_BIG;

        BigDecimal transferAmount = amount.setScale(2, RoundingMode.FLOOR);

        boolean sourceExists = accountDao.exists(config.getContextProvider(), sourceId);
        if (!sourceExists)
            throw AccountServiceException.accountNotFound(sourceId);

        boolean targetExists = accountDao.exists(config.getContextProvider(), targetId);
        if (!targetExists)
            throw AccountServiceException.accountNotFound(targetId);

        ThrowingConsumer<ConfigurationWrapper, AccountServiceException> lockingAction =
            transactionConfig -> {
                ContextProvider transactionCtx = transactionConfig.getContextProvider();

                List<AccountDto> accounts = accountDao.findForUpdate(transactionCtx, sourceId, targetId);

                AccountDto source = null;
                AccountDto target = null;
                for (AccountDto account: accounts) {
                    if (account.getId().equals(sourceId))
                        source = account;
                    else
                        target = account;
                }

                if (source == null || target == null)
                    throw AccountServiceException.ACCOUNT_FETCH_FAILED;

                BigDecimal sourceAmount = source.getAmount();
                if (sourceAmount.compareTo(transferAmount) < 0)
                    throw AccountServiceException.notEnoughFunds(sourceId, transferAmount);

                BigDecimal targetAmount = target.getAmount();

                BigDecimal updatedTargetAmount = targetAmount.add(transferAmount);
                if (updatedTargetAmount.compareTo(MAX_AMOUNT) > 0)
                    throw AccountServiceException.totalTargetAmountTooBig(targetId);

                BigDecimal updatedSourceAmount = sourceAmount.subtract(transferAmount);

                accountDao.updateAmount(transactionCtx, source, updatedSourceAmount);
                accountDao.updateAmount(transactionCtx, target, updatedTargetAmount);
            };

        config.withTransaction(lockingAction);
    }
}
