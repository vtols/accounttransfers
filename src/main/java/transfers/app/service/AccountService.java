package transfers.app.service;

import transfers.app.model.Account;
import transfers.app.service.error.AccountServiceException;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

public interface AccountService {

    Account createAccount(BigDecimal amount) throws AccountServiceException;

    Optional<Account> getAccount(UUID id) throws AccountServiceException;

    void transfer(UUID sourceId, UUID targetId, BigDecimal amount) throws AccountServiceException;

}
