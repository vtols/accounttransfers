package transfers.app.dao;

import transfers.app.model.AccountDto;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface AccountDao {

    AccountDto create(ContextProvider ctx, UUID id, BigDecimal initialAmount);

    boolean exists(ContextProvider ctx, UUID id);

    Optional<AccountDto> findById(ContextProvider ctx, UUID id);

    List<AccountDto> findForUpdate(ContextProvider ctx, UUID... ids);

    boolean updateAmount(ContextProvider ctx, AccountDto account, BigDecimal amount);

}
