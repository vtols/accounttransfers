package transfers.app.dao;

import org.jooq.*;
import transfers.app.model.AccountDto;

import java.math.BigDecimal;
import java.util.UUID;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;

public class AccountTable {

    public static final Table<Record> TABLE_NAME = table("ACCOUNT");

    public static final Field<UUID> ID = field("ID", UUID.class);

    public static final Field<BigDecimal> AMOUNT = field("AMOUNT", BigDecimal.class);

    public static final Field<Long> VERSION = field("VERSION", Long.class);

    public static final RecordMapper<Record3<UUID, BigDecimal, Long>, AccountDto> ACCOUNT_MAPPER =
            record -> new AccountDto(record.get(ID), record.get(AMOUNT), record.get(VERSION));

}
