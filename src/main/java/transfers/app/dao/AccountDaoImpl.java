package transfers.app.dao;

import transfers.app.model.AccountDto;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.jooq.impl.DSL.count;

public class AccountDaoImpl implements AccountDao {

    @Override
    public AccountDto create(ContextProvider ctx, UUID id, BigDecimal initialAmount) {
        ctx.getDSL()
            .insertInto(AccountTable.TABLE_NAME, AccountTable.ID, AccountTable.AMOUNT, AccountTable.VERSION)
            .values(id, initialAmount, 0L)
            .execute();

        return new AccountDto(id, initialAmount, 0L);
    }

    @Override
    public boolean exists(ContextProvider ctx, UUID id) {
        int flag = ctx.getDSL()
            .select(count())
            .from(AccountTable.TABLE_NAME)
            .where(AccountTable.ID.eq(id))
            .execute();

        return flag != 0;
    }

    @Override
    public Optional<AccountDto> findById(ContextProvider ctx, UUID id) {
        return ctx.getDSL()
            .select(AccountTable.ID, AccountTable.AMOUNT, AccountTable.VERSION)
            .from(AccountTable.TABLE_NAME)
            .where(AccountTable.ID.eq(id))
            .fetchOptional(AccountTable.ACCOUNT_MAPPER);
    }

    @Override
    public List<AccountDto> findForUpdate(ContextProvider ctx, UUID... ids) {
        return ctx.getDSL()
                .select(AccountTable.ID, AccountTable.AMOUNT, AccountTable.VERSION)
                .from(AccountTable.TABLE_NAME)
                .where(AccountTable.ID.in(ids))
                .forUpdate()
                .fetch(AccountTable.ACCOUNT_MAPPER);
    }

    @Override
    public boolean updateAmount(ContextProvider ctx, AccountDto account, BigDecimal amount) {
        int updated = ctx.getDSL()
                .update(AccountTable.TABLE_NAME)
                .set(AccountTable.AMOUNT, amount)
                .set(AccountTable.VERSION, AccountTable.VERSION.add(1))
                .where(AccountTable.ID.eq(account.getId()))
                .and(AccountTable.VERSION.eq(account.getVersion()))
                .execute();

        return updated != 0;
    }

}
