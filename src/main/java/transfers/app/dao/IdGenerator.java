package transfers.app.dao;

import java.util.UUID;

public interface IdGenerator {
    IdGenerator DEFAULT_ID_GENERATOR = UUID::randomUUID;

    UUID generateNewId();
}
