package transfers.app.dao;


import org.jooq.Configuration;
import org.jooq.impl.DSL;
import transfers.app.function.ThrowingConsumer;

public class ConfigurationWrapperImpl implements ConfigurationWrapper {

    private Configuration configuration;

    public ConfigurationWrapperImpl(Configuration configuration) {
        this.configuration = configuration;
    }

    public ContextProvider getContextProvider() {
        return () -> DSL.using(configuration);
    }

    public <E extends Exception> void withTransaction(ThrowingConsumer<ConfigurationWrapper, E> transactionActions) {
        DSL.using(configuration).transaction(ctx -> {
            ConfigurationWrapperImpl transactionConfigWrapper = new ConfigurationWrapperImpl(ctx);
            transactionActions.accept(transactionConfigWrapper);
        });
    }

}
