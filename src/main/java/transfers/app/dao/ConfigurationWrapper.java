package transfers.app.dao;

import transfers.app.function.ThrowingConsumer;

public interface ConfigurationWrapper {

    ContextProvider getContextProvider();

    <E extends Exception> void withTransaction(ThrowingConsumer<ConfigurationWrapper, E> transactionActions) throws E;

}
