package transfers.app.dao;

import org.jooq.DSLContext;

public interface ContextProvider {
    DSLContext getDSL();
}
